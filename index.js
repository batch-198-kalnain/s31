/*

	What is a client?

	A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server.

	What is a server?

	A server is able to host and develiver resources requested by a client. In fact, a single server can handle mutiple clients.

	What is Node.js?

	Node.js is a runtime environment which allows us to create/develop backend/server-side application with Javascript. Because by default, Javascript was conceptualized solely to the front end.

	Why is NodeJS popular?

	Performance - NodeJS is one of the most performing enviroment for creating backend application with JS.

	Familiarity - Since NodeJS is built and uses JS as its language, it is very familiar for most developers.

	NPM - Node Package Manager is the largest registry for node packages. Packages are bits of programs, methods, functions, codes that greatly help in the development of an application.

*/

// console.log("Hello World!");

let http = require("http");
// require() is a built in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application.

// http is a default package that comes with NodeJS. It allows us to use methods that let us create servers.

//http is a module. Modules are packages we imported. 
//Modules are objects that contain codes, methods or data.

//The http module let us create a server which is able to communicate with a client trhorugh the use of Hypertext Transfer Protocol. 

// protocol to client-server communication - http://localhost:4000

// console.log(http);

http.createServer(function(request, response){

	/*
		createServer() is a method from the http module that allows us to handle requests and responses from a client and a server respectively.

		.createServer() method takes a function argument which is able to receive 2 object. The request object which contains details of the request from the client. The response object which containes details of the response from the server. The createServer() method ALWAYS receives the request object first before the response.

		response.writeHead() - is a method of the response object. It allows us to add headers to our response. Headers are additional information about our response. We have 2 arguments in our writeHead() method. The first is the HTTP Status Code. An HTTP status is just a numerical code to let the client know about the status of their request. 200, means OK, 404 means the resource cannot be found. Content-Type is one of the most recognizable headers It simply pertains to the data type of our response.

	*/

	/*

		Servers can actually respond differently with different requests.

		We start our request with our URL. A client can start a different request with a different url.
		htpp://localhost:4000/ is not the same with http://localhost:4000/profile

		/ = url endpoint(default)
		/profile = url endpoint

		We can differentiate by their endpoints, we should be able to respond differently to different endpoints.

		Information about the URL endpoint of the request is in the request object.

		request.url contains the URL endpoint
		ex.
		/ = url endpoint(default) - request URL: http://localhost:4000

		/profile = url endpoint(default) - request URL: http://localhost:4000/profile
	      ////////////////////////////////////////////////////////
		 //Note: Different request requires different response.//
		////////////////////////////////////////////////////////

		The process or way to respond differently to a request is called a route.
	*/

	// console.log(request.url); //contains the endpoint

	if(request.url === "/"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		/*

			response.end() - basically show the last response before it ends, and nothing follows.
			Note: response.end() only receive string type arguments.

		*/
		response.end("Hello from our first Server! This is from / endpoint");
	} else if(request.url === "/profile") {
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Hi I'm Anonymous");
	}

}).listen(4000);

	/*

		.listen() allows us to assign a port to our server. This will allow us to serve our index.js server in our local machine assigned to port 4000. There are several task and processes on our computer/machine that run on different port numbers.

		hypertext transfer protocol - http://localhost:4000/ - server

		localhost: -- local machine : 4000 -- current port assigned to our server

		4000,4040,8000,5000,3000, 4200 - usually used for web development 

	*/

console.log("Server is running on localhost:4000!");
